using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    [SerializeField] Animator animatorBox;
    [SerializeField] Grid3D storage;


    private System.Action _onCompletePack = null;

    public void AddFruits(List<GameObject> fruits, System.Action OnCompletePack = null)
    {
        if (fruits == null || fruits.Count <= 0) return;

        _onCompletePack = OnCompletePack;

        for (int i = 0; i < fruits.Count; i++)
        {
            int index = i;

            this.ActionWaitTime(index * 0.15f, () => 
            {
                storage.AddElement(fruits[index]);
            });
        }

        this.ActionWaitTime(fruits.Count * 0.2f + 0.2f, () =>
        {
            animatorBox.SetTrigger("Close");
        });
    }

    public void Close()
    {
        while (!storage.IsEmpty)
        {
            GameObject fruit = storage.RemoveElement();

            GameManager.Instance.fruitSpawner.pool.Release(fruit);
        }

        _onCompletePack?.Invoke();
    }
}
