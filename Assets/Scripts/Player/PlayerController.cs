using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Singleton<PlayerController>
{
    [SerializeField] private float speedMove = 5f;
    [SerializeField] private float speedRotate = 5f;

    [SerializeField] private Rigidbody rb = default;
    [SerializeField] private PlayerModel playerModel = default;

    [SerializeField] Grid3D gridHolding = default;
    [SerializeField] GameObject UIMaxHolding = default;

    private Vector3 _inputMovement = Vector3.zero;

    public bool IsCarry => !gridHolding.IsEmpty;

    public bool IsMaxCarry => gridHolding.IsMax; 

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        _inputMovement = UIManager.Instance.InputJoystick;

        if (_inputMovement.magnitude > 0.1f)
        {
            playerModel.PlayAnimationMove(IsCarry);

            Quaternion targetRotation = Quaternion.LookRotation(_inputMovement);
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, targetRotation, speedRotate * Time.deltaTime);

            return;
        }

        playerModel.PlayAnimationIdle(IsCarry);
    }


    public void CollectFruit(GameObject fruit)
    {
        gridHolding.AddElement(fruit);
        UIMaxHolding.SetActive(gridHolding.IsMax);
    }

    public GameObject GetFruit() 
    {
        if (!IsCarry) return null;

        GameObject fruit = gridHolding.RemoveElement();
        UIMaxHolding.SetActive(gridHolding.IsMax);

        return fruit;
    }
}
