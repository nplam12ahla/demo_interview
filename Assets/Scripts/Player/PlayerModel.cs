using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : MonoBehaviour
{
    [SerializeField] private Animator animator = default;

    public void PlayAnimationIdle(bool isCarry)
    {
        animator.SetBool(GameDefine.NAME_ANIM_IDLE, !isCarry);
        animator.SetBool(GameDefine.NAME_ANIM_CARRYMOVE, false);
        animator.SetBool(GameDefine.NAME_ANIM_MOVE, false);
    }

    public void PlayAnimationMove(bool isCarry)
    {
        animator.SetBool(GameDefine.NAME_ANIM_IDLE, !isCarry);
        animator.SetBool(GameDefine.NAME_ANIM_CARRYMOVE, isCarry);
        animator.SetBool(GameDefine.NAME_ANIM_MOVE, !isCarry);
    }

    public Animator Animator => animator;
}
