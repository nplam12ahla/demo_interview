using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClientMovement : MonoBehaviour
{
    [SerializeField] private PlayerModel model = default;
    [SerializeField] private NavMeshAgent agent = default;

    private Vector2 Velocity;
    private Vector2 SmoothDeltaPosition;
    private Grid3D _gridHoding = default;
    private Transform _boxHolding = default;

    public void SetUp(Grid3D grid, Transform box)
    {
        _gridHoding = grid;
        _boxHolding = box;
    }

    public bool IsCarry => (_gridHoding != null && !_gridHoding.IsEmpty) || (_boxHolding != null &&
        _boxHolding.childCount > 0);

    private void Start()
    {
        agent.updatePosition = false;
        agent.updateRotation = true;
        model.Animator.applyRootMotion = true;
    }

    private void OnAnimatorMove()
    {
        Vector3 rootPosition = model.Animator.rootPosition;
        rootPosition.y = agent.nextPosition.y;
        transform.position = rootPosition;
        agent.nextPosition = rootPosition;
    }

    private void Update()
    {
        SyncAnimatorAndAgent();
    }

    private void SyncAnimatorAndAgent()
    {
        if (!agent.enabled) return;

        Vector3 worldDeltaPosition = agent.nextPosition - transform.position;
        worldDeltaPosition.y = 0;

        float dx = Vector3.Dot(transform.right, worldDeltaPosition);
        float dy = Vector3.Dot(transform.forward, worldDeltaPosition);
        Vector2 deltaPosition = new Vector2(dx, dy);

        float smooth = Mathf.Min(1, Time.deltaTime / 0.1f);
        SmoothDeltaPosition = Vector2.Lerp(SmoothDeltaPosition, deltaPosition, smooth);

        Velocity = SmoothDeltaPosition / Time.deltaTime;
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            Velocity = Vector2.Lerp(Vector2.zero, Velocity, agent.remainingDistance);
        }

        bool shouldMove = Velocity.magnitude > 0.5f && agent.remainingDistance > agent.stoppingDistance;

        if (shouldMove)
            model.PlayAnimationMove(IsCarry);
        else
            model.PlayAnimationIdle(IsCarry);

        if (agent.remainingDistance <= agent.stoppingDistance && !agent.isStopped)
        {
            agent.isStopped = true;
            agent.enabled = false;

            _onMoveToTargetComplete?.Invoke();
        }
    }


    private System.Action _onMoveToTargetComplete = null;

    public void MoveToTarget(Vector3 target, System.Action OnMoveToTargetComplete = null)
    {
        agent.enabled = true;
        agent.isStopped = false;
        agent.SetDestination(target);
        _onMoveToTargetComplete = OnMoveToTargetComplete;
    }

}
