using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class ClientController : MonoBehaviour
{
    [SerializeField] public Transform boxTransform = default;
    [SerializeField] private Grid3D gridHoding = default;
    [SerializeField] private ClientMovement clientMovement = default;

    [SerializeField] GameObject objUiIconCheckout = default;
    [SerializeField] GameObject objUiIconFruit = default;
    [SerializeField] GameObject objUiIconSmile = default;

    [SerializeField] Text txtTargetFruit = default;

    private int _targetFruit = 5;
    private int _currentFruit = 0;

    public int AmountFruit => _targetFruit;

    public ClientMovement ClientMovement => clientMovement;

    public Grid3D Storage => gridHoding;

    public bool IsCarry => !gridHoding.IsEmpty;

    private void Start()
    {
        clientMovement.SetUp(gridHoding, boxTransform);
    }

    private void OnEnable()
    {
        _targetFruit = Random.Range(1, 6);
        _currentFruit = 0;

        this.ActionWaitTime(0.1f, () =>
        {
            CollectFruit();
        });
    }   

    private void OnDisable()
    {
        if (boxTransform.childCount > 0)
        {
            GameManager.Instance.boxSpawner.pool.Release(boxTransform.GetChild(0).gameObject);
        }
    }

    private bool _canCollect = true;

    public void CollectFruit()
    {
        SetUiIcon(true, false, false);
        txtTargetFruit.text = $"{_currentFruit}/{_targetFruit}";
        Shelf shelfTarget = GameManager.Instance.GetShelf();

        if (shelfTarget == null) return;

        Transform slot = shelfTarget.GetTransQueueFree();
        slot.gameObject.SetActive(true);

        clientMovement.MoveToTarget(slot.position, () =>
        {
            transform.LookAt(shelfTarget.transform);

            this.ActionWhile(() => _currentFruit < _targetFruit, () =>
            {
                if (_canCollect)
                {
                    _canCollect = false;

                    this.ActionWaitTime(0.2f, () => _canCollect = true);

                    GameObject fruit = shelfTarget.Storage.RemoveElement();

                    if (fruit)
                    {
                        gridHoding.AddElement(fruit);
                        _currentFruit++;
                        txtTargetFruit.text = $"{_currentFruit}/{_targetFruit}";
                    }
                }
            }, () =>
            {
                _canCollect = false;
                slot.gameObject.SetActive(false);

                CheckOut();
            });
        });
    }

    public void CheckOut()
    {
        SetUiIcon(false, true, false);
        GameManager.Instance.CheckoutCounter.AddClientToQueue(this);
    }

    public void MoveToEndPoint()
    {
        SetUiIcon(false, false, true);

        clientMovement.MoveToTarget(GameManager.Instance.transformSpawn.position, () =>
        {
            gameObject.SetActive(false);
        });
    }

    private void SetUiIcon(bool isTarget, bool isCasher, bool isEmoji)
    {
        objUiIconCheckout.SetActive(isCasher);
        objUiIconFruit.SetActive(isTarget);
        objUiIconSmile.SetActive(isEmoji);
    }
}
