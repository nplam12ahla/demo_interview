using DG.Tweening;
using NPLTemplate;
using UnityEngine;
using UnityEngine.UI;

public class BuildingLock : MonoBehaviour
{
    [SerializeField] private int remainCost = 0;

    [SerializeField] Type type;

    [SerializeField] Transform tranBuidling = default;

    [SerializeField] GameObject objVfxBuilding = default;

    [SerializeField] Text txtMoney = default;

    private bool _isCoolDown = false;

    private float _timeWait = 0;


    enum Type
    {
        Plant,
        Shelf
    }

    private void OnEnable()
    {
        txtMoney.text = remainCost.ToString();
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(GameDefine.TAG_PLAYER) && GameManager.Instance.Money > 0)
        {
            _timeWait += Time.deltaTime;

            if (_timeWait >= 0.5f && !_isCoolDown)
            {
                _isCoolDown = true;

                this.ActionWaitTime(0.15f, () => _isCoolDown = false);

                GameObject money = GameManager.Instance.moneySpawner.pool.Get();
                money.transform.position = other.transform.position + new Vector3(0, 1, 0);

                money.transform.DOMove(transform.position, 0.15f).OnComplete(() =>
                {
                    remainCost--;
                    txtMoney.text = remainCost.ToString();
                    GameManager.Instance.Money--;

                    if (remainCost <= 0)
                    {
                        ShowBuilding();

                        if (type == Type.Shelf)
                        {
                            GameManager.Instance.AddShelfBase(tranBuidling.GetComponent<Shelf>());
                        }
                    }
                });
            }
        }
    }

    private void ShowBuilding()
    {
        GameObject vfx = Instantiate(objVfxBuilding);
        tranBuidling.localScale = Vector3.one * 0.5f;

        vfx.transform.position = objVfxBuilding.transform.position;
        vfx.SetActive(true);

        tranBuidling.gameObject.SetActive(true);

        tranBuidling.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack);

        Destroy(gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(GameDefine.TAG_PLAYER))
        {
            _timeWait = 0;
        }
    }
}
