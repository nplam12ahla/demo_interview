using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private VariableJoystick variableJoystick = default;

    public Vector3 InputJoystick => new Vector3(variableJoystick.Horizontal, 0, variableJoystick.Vertical);

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }
}
