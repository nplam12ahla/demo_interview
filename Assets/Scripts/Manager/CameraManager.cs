using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [Header("Element")]
    [SerializeField] private Transform target;
    Vector3 offset;

    void Start()
    {
        offset = transform.position - target.position;
    }

    void LateUpdate()
    {
        transform.position = target.position + offset;
    }
}