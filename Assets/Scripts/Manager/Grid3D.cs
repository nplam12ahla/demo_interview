using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NPLTemplate
{
    public class Grid3D : MonoBehaviour
    {
        [SerializeField] private Vector3 offetXZY = Vector3.zero;

        [SerializeField] private Vector3 gridXYZSize;

        [SerializeField] private Vector3 gridXYZSpace;

        private Dictionary<GameObject, Vector3> _elements;

        private Vector3 _currentPosition = Vector3.zero;

        private void Start()
        {
            _elements = new Dictionary<GameObject, Vector3>();
        }

        public bool AddElement(GameObject obj)
        {
            if (IsMax) return false;

            _elements.Add(obj, _currentPosition);

            obj.transform.SetParent(transform);

            obj.transform.DOLocalMove(new Vector3(_currentPosition.x * gridXYZSpace.x,
                                                      _currentPosition.y * gridXYZSpace.y,
                                                      _currentPosition.z * gridXYZSpace.z) + offetXZY, 0.2f);

            obj.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);

            obj.transform.eulerAngles = Vector3.zero;

            if (gridXYZSize.x == 0 && gridXYZSize.z == 0 && gridXYZSize.y != 0)
            {
                _currentPosition += new Vector3(0, 1, 0);
            }
            else
            {
                _currentPosition += new Vector3(1, 0, 0);

                if (_currentPosition.x == gridXYZSize.x)
                    _currentPosition += new Vector3(-gridXYZSize.x, 0, 1);

                if (_currentPosition.z == gridXYZSize.z)
                    _currentPosition += new Vector3(0, 1, -gridXYZSize.z);
            }

            return true;
        }

        public GameObject RemoveElement()
        {
            if (_elements.Count == 0) return null;

            GameObject obj = _elements.Keys.Last();

            _currentPosition = _elements[obj];

            _elements.Remove(obj);

            return obj;
        }

        public List<GameObject> RemoveAllElement()
        {
            if (IsEmpty) return null;

            List<GameObject> list = new List<GameObject>();

            while (!IsEmpty)
            {
                GameObject obj = _elements.Keys.Last();

                _currentPosition = _elements[obj];

                _elements.Remove(obj);

                list.Add(obj);
            }

            return list;
        }

        public bool IsEmpty => _elements.Count == 0 || _elements == null;

        public bool IsMax => _currentPosition.y == gridXYZSize.y;
    }
}