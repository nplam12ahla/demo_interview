using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    public Transform transformSpawn = default;

    public Spawner fruitSpawner = default;
    public Spawner boxSpawner = default;
    public Spawner moneySpawner = default;

    [SerializeField] Text txtMoney = default;

    [SerializeField] List<Shelf> shelves = new List<Shelf>();

    [SerializeField] private float timeSpawnClient = 4f;
    [SerializeField] List<ClientController> clientControllers;


    [SerializeField] CheckoutCounter checkoutCounter;

    public void AddShelfBase(Shelf shelf)
    {
        shelves.Add(shelf);
    }

    private int _money = 0;

    public int Money
    {
        get
        {
            return _money;
        }
        set
        {
            _money = value;
            txtMoney.text = _money.ToString(); 
        }
    }


    public CheckoutCounter CheckoutCounter => checkoutCounter;

    public Shelf GetShelf()
    {
        List<Shelf> find = shelves.FindAll(x => x.GetTransQueueFree());

        if (find == null || find.Count == 0) return null;

        Shelf shelf = find.FirstOrDefault(x => !x.Storage.IsEmpty);

        return shelf ? shelf : find[Random.Range(0, find.Count)]; 
    }

    private void Awake()
    {
        Money = 0;
    }

    private void Start()
    {
        InvokeRepeating(nameof(Spawn), 0, timeSpawnClient);
    }

    private void Spawn()
    {
        List<ClientController> clients = clientControllers.FindAll(x => !x.gameObject.activeSelf);

        if (clients == null || clients.Count <= 0) return;
        clients[Random.Range(0,clients.Count)].gameObject.SetActive(true);
    }
}
