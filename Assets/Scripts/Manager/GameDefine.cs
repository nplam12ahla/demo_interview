using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDefine : MonoBehaviour
{
    public static string NAME_ANIM_IDLE = "IsEmpty";
    public static string NAME_ANIM_MOVE = "IsMove";
    public static string NAME_ANIM_CARRYMOVE = "IsCarryMove";
    public static string NAME_ANIM_PLANT_REFILL = "TomatoRefill";
    public static string NAME_ANIM_PLANT_IDLE = "TomatoIdle";

    public static string TAG_PLAYER = "Player";

}
