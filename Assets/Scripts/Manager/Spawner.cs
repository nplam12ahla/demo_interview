using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

namespace NPLTemplate
{
    public class Spawner : MonoBehaviour
    {

        public ObjectPool<GameObject> pool;
        public GameObject prefabs;


        void Awake()
        {
            pool = new ObjectPool<GameObject>(Create, OnTakeGameObjectFromPool, OnReturnBulletToPool, null, true);
        }

        private GameObject Create()
        {
            GameObject clone = Instantiate(prefabs, transform);

            return clone;
        }

        private void OnTakeGameObjectFromPool(GameObject obj)
        {
            obj.SetActive(true);
        }

        private void OnReturnBulletToPool(GameObject obj)
        {
            obj.SetActive(false);
            obj.transform.SetParent(transform);
        }
    }
}