using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TomatoPlant : MonoBehaviour
{
    [SerializeField] float timeSpawnFruit = 1f;
    [SerializeField] float cooldownCollect = 0.25f;
    [SerializeField] Animation animPlant = default;
    [SerializeField] Spawner fruitSpawner = default;
    [SerializeField] List<Transform> transSpawnFruits = default;

    private bool _canCollect = true;
    private bool _isSpawning = false;

    private void OnEnable()
    {
        transSpawnFruits.ForEach(x =>
        {
            SpawnFruit(x);
        });
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_canCollect) return;

        if (!other.CompareTag(GameDefine.TAG_PLAYER)) return;

        if (PlayerController.Instance.IsMaxCarry) return;

        Transform fruit = transSpawnFruits.Find(x => x.childCount != 0);

        if (!fruit) return;

        _canCollect = false;

        this.ActionWaitTime(cooldownCollect, () => _canCollect = true);

        PlayerController.Instance.CollectFruit(fruit.GetChild(0).gameObject);
        SpawnFruit(fruit);
    }

    private void SpawnFruit(Transform pos)
    {
        animPlant.Play(GameDefine.NAME_ANIM_PLANT_REFILL);

        this.ActionWaitTime(timeSpawnFruit, () =>
        {
            Transform clone = fruitSpawner.pool.Get().transform;
            clone.SetParent(pos);
            clone.localPosition = Vector3.zero;

            if (transSpawnFruits.All(x => x.childCount != 0))
                animPlant.Play(GameDefine.NAME_ANIM_PLANT_IDLE);

        });
    }
}
