using DG.Tweening;
using NPLTemplate;
using System.Collections.Generic;
using UnityEngine;

public class CheckoutCounter : MonoBehaviour
{
    [SerializeField] private Transform transInArea = default;

    [SerializeField] private Transform boxSpawnPosition = default;

    [SerializeField] private List<Transform> slotPosition = default;

    [SerializeField] private Grid3D storageMoney = default;

    private List<ClientController> clientsQueue = new List<ClientController>();

    private Tween _tweenScaleArea = null;

    private Box _currentBox = null;

    private bool _canTakeMoney = true;

    private bool _isInArea = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GameDefine.TAG_PLAYER))
        {
            _tweenScaleArea.Kill();
            _tweenScaleArea = transInArea.DOScale(new Vector3(1.075f, 1, 0.97f), 0.25f).SetEase(Ease.InOutQuad);
            _isInArea = true;
        }
    }



    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(GameDefine.TAG_PLAYER) && !storageMoney.IsEmpty && _canTakeMoney)
        {
            _canTakeMoney = false;

            this.ActionWaitTime(0.1f, () => _canTakeMoney = true);

            GameObject money = storageMoney.RemoveElement();

            money.transform.SetParent(other.transform);
            money.transform.DOLocalMove(new Vector3(0, 1, 0), 0.2f).OnComplete(() =>
            {
                GameManager.Instance.Money++;
                GameManager.Instance.moneySpawner.pool.Release(money);
            });
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(GameDefine.TAG_PLAYER))
        {
            _tweenScaleArea.Kill();
            _tweenScaleArea = transInArea.DOScale(Vector3.one, 0.25f).SetEase(Ease.InOutQuad);
            _isInArea = false;
        }
    }

    public void AddClientToQueue(ClientController client)
    {
        clientsQueue.Add(client);
        ClientMoveInQueue();
    }

    public void RemoveClientFromQueue()
    {
        if (clientsQueue.Count <= 0) return;
        clientsQueue.RemoveAt(0);
        ClientMoveInQueue();
    }

    private bool _isHandling = false;

    private void ClientMoveInQueue()
    {
        for (int i = 0; i < clientsQueue.Count; i++)
        {
            int index = i;
            clientsQueue[index].ClientMovement.MoveToTarget(slotPosition[index].position, () =>
            {
                if (index == 0 && !_isHandling)
                {
                    _isHandling = true;
                    this.ActionWaitUntil(() => _isInArea, () =>
                    {
                        Checkout(clientsQueue[index]);
                    });
                }
            });
        }
    }

    private void Checkout(ClientController client)
    {
        client.transform.LookAt(transform);

        PackFruit(client);
    }

    private void PackFruit(ClientController client)
    {
        _currentBox = GameManager.Instance.boxSpawner.pool.Get().GetComponent<Box>();
        _currentBox.transform.position = boxSpawnPosition.position;
        _currentBox.transform.rotation = boxSpawnPosition.rotation;
        _currentBox.transform.localScale = Vector3.zero;


        _currentBox.transform.DOScale(Vector3.one, 0.75f)
                             .SetEase(Ease.OutBack)
                             .OnComplete(() =>
                             {
                                 _currentBox.AddFruits(client.Storage.RemoveAllElement(), () =>
                                 {
                                     _currentBox.transform.SetParent(client.boxTransform);

                                     _currentBox.transform.DOLocalMove(Vector3.zero, 0.25f);
                                     _currentBox.transform.DOLocalRotate(Vector3.zero, 0.25f).OnComplete(() =>
                                     {
                                         ReceiveCash(client);
                                     });
                                 });
                             });
    }
    private void ReceiveCash(ClientController client)
    {
        for (int i = 0; i < client.AmountFruit * 3; i++)
        {
            int index = i;

            this.ActionWaitTime(index * 0.2f, () =>
            {

                GameObject money = GameManager.Instance.moneySpawner.pool.Get();
                money.transform.position = client.boxTransform.position;
                storageMoney.AddElement(money);
            });
        }

        this.ActionWaitTime(client.AmountFruit * 3 * 0.2f + 0.2f, () =>
        {
            client.MoveToEndPoint();
            RemoveClientFromQueue();
            _isHandling = false;
        });
    }


}
