using DG.Tweening;
using NPLTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelf : MonoBehaviour
{
    [SerializeField] private Transform transInArea = default;
    [SerializeField] private Grid3D gridHolding = default;
    [SerializeField] private List<Transform> transQueueClient = default;

    private Tween _tweenScaleArea = null;
    private bool _canPutFruit = true;

    public Grid3D Storage => gridHolding;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(GameDefine.TAG_PLAYER)) return;

        _tweenScaleArea.Kill();
        _tweenScaleArea = transInArea.DOScale(new Vector3(1.1f, 1, 1.1f), 0.2f).SetEase(Ease.InOutQuad);
    }

    private void OnTriggerStay(Collider other)
    {

        if (other.CompareTag(GameDefine.TAG_PLAYER) &&
                                       _canPutFruit &&
                  PlayerController.Instance.IsCarry &&
                                !gridHolding.IsMax)
        {
            _canPutFruit = false;

            this.ActionWaitTime(0.25f, () => _canPutFruit = true);

            GameObject fruit = PlayerController.Instance.GetFruit();

            if (fruit)
                gridHolding.AddElement(fruit);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(GameDefine.TAG_PLAYER)) return;

        _tweenScaleArea.Kill();
        _tweenScaleArea = transInArea.DOScale(Vector3.one, 0.2f).SetEase(Ease.InOutQuad);
    }

    public Transform GetTransQueueFree()
    {
        List<Transform> slotFree = transQueueClient.FindAll(x => !x.gameObject.activeSelf);

        if (slotFree == null || slotFree.Count <=0 ) return null;

        return slotFree[Random.Range(0, slotFree.Count)];
    }
}
